#This CMakeLists.txt was automatically generated.
#You can add things but not change existing things.
#For instance don't change the variable names !


cmake_minimum_required(VERSION 3.4.3)

if(COMMAND cmake_policy)
	if( POLICY CMP0003 )
        cmake_policy(SET CMP0003 NEW)
	endif( POLICY CMP0003 )
	if( POLICY CMP0015 )
        cmake_policy(SET CMP0015 NEW)
    endif( POLICY CMP0015 )
endif(COMMAND cmake_policy)

project( displayComments )


if( NOT CMAKE_VERBOSE_MAKEFILE )
    set( CMAKE_VERBOSE_MAKEFILE ON )
endif( NOT CMAKE_VERBOSE_MAKEFILE )

#CUSTOM SETTINGS
# If global settings are not specifically enabled, local settings prevails
set( MY_GLOBAL_SETTINGS_IS_SET False CACHE BOOL "If False: global settings are not enabled, and local settings prevails. Else global settings defined in the file whose path is specified in the environement variable $CMAKE_CLANG_COMPILE_VARIABLES are used." )
if( MY_GLOBAL_SETTINGS_IS_SET )
    set( CUSTOM_CMAKE_SETTINGS $ENV{CMAKE_CLANG_COMPILE_VARIABLES} )
endif( MY_GLOBAL_SETTINGS_IS_SET )


if( NOT EXISTS ${CUSTOM_CMAKE_SETTINGS} )
    message( "Warning: CMAKE_CLANG_COMPILE_VARIABLES not defined, switch to default configuration." )
    set( ENV{CMAKE_CLANG_COMPILE_VARIABLES} ${PROJECT_SOURCE_DIR}/CMakeCompileVars.clang.cmake.example )
endif( NOT EXISTS ${CUSTOM_CMAKE_SETTINGS} )
message( "The environement variable CMAKE_CLANG_COMPILE_VARIABLES is set to " $ENV{CMAKE_CLANG_COMPILE_VARIABLES} )


include( $ENV{CMAKE_CLANG_COMPILE_VARIABLES} )
set( LLVM_DIR ${MY_LLVM_COMPILED_PATH} )

#C compiler
set( CMAKE_C_COMPILER ${CC_COMPILER} )

if( NOT FLAGS )
	set( FLAGS "" )
endif( NOT FLAGS )

set( CMAKE_C_FLAGS_DEBUG "${CC_DEBUG_FLAGS} ${FLAGS}" )
set( CMAKE_C_FLAGS_RELEASE "${CC_OPTIMIZATION_FLAGS} ${FLAGS}" )

#C++ compiler
set( CMAKE_CXX_COMPILER ${CXX_COMPILER} )

set( CMAKE_CXX_FLAGS_DEBUG "${CXX_DEBUG_FLAGS} ${FLAGS}" )
set( CMAKE_CXX_FLAGS_RELEASE "${CXX_OPTIMIZATION_FLAGS} ${FLAGS}" )

#message( "flags: " ${FLAGS} )

if ( NOT CMAKE_BUILD_TYPE )
	set ( CMAKE_BUILD_TYPE Debug )
endif ( NOT CMAKE_BUILD_TYPE )

#Find sources
file( GLOB displayComments_files ${PROJECT_SOURCE_DIR}/[^~.]*.[cC] ${PROJECT_SOURCE_DIR}/[^~.]*.[cC]?? )
file( GLOB displayComments_headers ${PROJECT_SOURCE_DIR}/[^~.]*.[thH] ${PROJECT_SOURCE_DIR}/[^~.]*.[thH]?? )

## Set CMAKE_MODULE_PATH which is a list
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/CMake)
#message( STATUS "CMAKE_MODULE_PATH is : ${CMAKE_MODULE_PATH}")


find_package(LLVM REQUIRED CONFIG
  PATHS ${MY_LLVM_COMPILED_PATH}/lib/cmake/llvm/
  NO_DEFAULT_PATH)

find_package(Clang REQUIRED CONFIG
  PATHS ${MY_LLVM_COMPILED_PATH}/lib/cmake/clang/
  NO_DEFAULT_PATH)


message(STATUS "Found LLVM ${LLVM_PACKAGE_VERSION}")
message(STATUS "Using LLVMConfig.cmake in: ${LLVM_DIR}")



#Program libraries
set( displayComments_libs
    PRIVATE
    ${llvm_libs}
    clang
    clangAST
    clangASTMatchers
    clangAnalysis
    clangBasic
    clangDriver
    clangEdit
    clangFrontend
    clangFrontendTool
    clangLex
    clangParse
    clangSema
    clangEdit
    clangRewrite
    clangRewriteFrontend
    clangStaticAnalyzerFrontend
    clangStaticAnalyzerCheckers
    clangStaticAnalyzerCore
    clangSerialization
    clangTooling
    clangToolingCore
    clangFormat
    )

# Find the libraries that correspond to the LLVM components
# that we wish to use
llvm_map_components_to_libnames(llvm_libs support core irreader)


#Program includes
include_directories(
  ${LLVM_INCLUDE_DIRS}
  ${CLANG_INCLUDE_DIRS}
  )

add_definitions(${LLVM_DEFINITIONS})

set(LLVM_LINK_COMPONENTS
    Support
    )

#Program library paths
link_directories(
  ${MY_LLVM_COMPILED_PATH}/lib
  ${MY_LLVM_COMPILED_PATH}/lib/clang/7.0.0/lib/linux/
  )

#Program
add_executable( displayComments ${displayComments_files} )

#Link
target_link_libraries( displayComments ${displayComments_libs} )
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/bin)
#INSTALL(TARGETS displayComments DESTINATION ${THE_BINS} )
