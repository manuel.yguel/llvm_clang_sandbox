//La simple fonction
int f( int /*< inline commentaire */a )
{///commentaire apres {

  ///Comment avant if
  if( a==0 )
  {
      return -1;
  }

  return a+1;
  /** commentaire
  en plusieures
  lignes */
}
