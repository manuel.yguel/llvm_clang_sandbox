#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Driver/Options.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "clang/AST/Attr.h"
#include "clang/AST/CommentCommandTraits.h"
#include "clang/AST/CommentVisitor.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/DeclLookups.h"
#include "clang/AST/DeclObjC.h"
#include "clang/AST/DeclOpenMP.h"
#include "clang/AST/DeclVisitor.h"
#include "clang/AST/LocInfoType.h"
#include "clang/AST/StmtVisitor.h"
#include "clang/AST/TypeVisitor.h"
#include "clang/Basic/Builtins.h"
#include "clang/Basic/Module.h"
#include "clang/Basic/SourceManager.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/Support/CommandLine.h"

#include <fstream>
#include <iostream>
#include <string>

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace clang::comments;
using namespace llvm;

class PrintCommentVisitor : public RecursiveASTVisitor<PrintCommentVisitor>
{
  private:
    ASTContext *astContext; // used for getting additional AST info

  public:
    explicit PrintCommentVisitor(CompilerInstance *CI)
        : astContext(&(CI->getASTContext())) // initialize private members
    {
    }

    virtual bool VisitDecl(Decl *D)
    {
        SourceManager &sm = astContext->getSourceManager();
        const RawComment *rc = astContext->getRawCommentForDeclNoCache(D);
        if (rc)
        {
            // Found comment!

            // Get location of comment
            /*
            SourceRange range = rc->getSourceRange();

            PresumedLoc startPos = sm.getPresumedLoc(range.getBegin());
            PresumedLoc endPos = sm.getPresumedLoc(range.getEnd());
            */

            // Get comment text
            std::string raw = rc->getRawText(sm);

            // Brief in term of doxygen "brief" definition
            // std::string brief = rc->getBriefText(ctx);

            // Display comments
            cout << "Provide only comment attached to declaration: " << raw << endl << endl;
        }

        return true;
    }

    /** To be called after the traversal of the entire code. */
    void displayComments( bool show_position=false )
    {
        SourceManager &sm = astContext->getSourceManager();

        cout << "Iterate over all comments" << endl;
        cout << "°°°°°°°°°°°°°°°°°°°°°°°°°" << endl;
        RawCommentList &cli = astContext->getRawCommentList();
        ArrayRef<RawComment *> comments = cli.getComments();
        for (int c = 0; c < comments.size(); ++c)
        {
            RawComment *rc = comments[c];
            std::string raw = rc->getRawText(sm);
            cout << "Comment: " << raw << endl;

            // Get location of comment
            SourceRange range = rc->getSourceRange();
            PresumedLoc startPos = sm.getPresumedLoc(range.getBegin());
            PresumedLoc endPos = sm.getPresumedLoc(range.getEnd());
            //File
            /*
            const char * filename = startPos.getFilename();
            */

            //Display line,column
            cout << "Comment start: L" << startPos.getLine() << " : C" << startPos.getColumn() << endl;
            //End is past the end or take into account the newline character
            cout << "Comment end: L" << endPos.getLine() << " : C" << (endPos.getColumn()-1) << endl;
            cout << endl << endl;
        }
        cout << "End iteration over all comments." << endl;
        cout << "================================" << endl << endl;
    }
};

class PrintCommentASTConsumer : public ASTConsumer
{
  private:
    PrintCommentVisitor visitor; // doesn't have to be private

  public:
    // override the constructor in order to pass CI
    explicit PrintCommentASTConsumer(CompilerInstance *CI)
        : visitor(CI) // initialize the visitor
    {
    }

    // override this to call the PrintCommentVisitor on the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context)
    {
        /* we can use ASTContext to get the TranslationUnitDecl, which is
             a single Decl that collectively represents the entire source file
           */
        visitor.TraverseDecl(Context.getTranslationUnitDecl());
        visitor.displayComments(true);
    }
};

class PrintCommentFrontendAction : public ASTFrontendAction
{
  public:
    typedef std::unique_ptr<ASTConsumer> ASTConsumer_unique_ptr_t;

  public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                           StringRef file)
    {
        return ASTConsumer_unique_ptr_t(
            new PrintCommentASTConsumer(&CI)); // pass CI pointer to ASTConsumer
    }
};

static cl::OptionCategory MyToolCategory("My tool options");

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...\n");

int main(int argc, const char **argv)
{
    // parse the command-line args passed to your code
    CommonOptionsParser op(argc, argv, MyToolCategory);
    // create a new Clang Tool instance (a LibTooling environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());

    // run the Clang Tool, creating a new FrontendAction (explained below)
    int result =
        Tool.run(newFrontendActionFactory<PrintCommentFrontendAction>().get());

    return result;
}