The aim of this project is to provide standalone, simple examples that use LLVM / CLANG to perform code analysis.

To compile the examples need llvm 7.0.
When developped, I used a compiled version of llvm from the sources.
The information about the version of the llvm sources is:
  * Repository Root: http://llvm.org/svn/llvm-project
  * Repository UUID: 91177308-0d34-0410-b5e6-96231b3b80d8
  * Revision: 331651

To compile the code you should edit the files named CMakeCompileVars.clang.cmake.example in each example repository.
You have to update the 2 CMake variables defined at the very begining of the CMakeCompileVars.clang.cmake.example:
  * MY_LLVM_COMPILED_PATH
  * MY_LLVM_USED_SOURCES
with the actual value where the llvm sources and compilation results are located on your computer.
