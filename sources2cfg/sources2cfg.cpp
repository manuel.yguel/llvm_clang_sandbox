#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Driver/Options.h"
#include "clang/Frontend/ASTConsumers.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendActions.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"

#include "llvm/Support/CommandLine.h"

#include "clang/Analysis/CFG.h"
#include "llvm/Support/DOTGraphTraits.h"
#include "llvm/Support/GraphWriter.h"
#include <fstream>
#include <string>

#include "cfg_pretty_printing.hpp"

using namespace std;
using namespace clang;
using namespace clang::driver;
using namespace clang::tooling;
using namespace llvm;

Rewriter rewriter;
int numFunctions = 0;

class ExampleVisitor : public RecursiveASTVisitor<ExampleVisitor>
{
  private:
    ASTContext *astContext; // used for getting additional AST info

  public:
    explicit ExampleVisitor(CompilerInstance *CI)
        : astContext(&(CI->getASTContext())) // initialize private members
    {
        rewriter.setSourceMgr(astContext->getSourceManager(),
                              astContext->getLangOpts());
    }

    virtual bool VisitFunctionDecl(FunctionDecl *func)
    {
        numFunctions++;

        string funcName = func->getNameInfo().getName().getAsString();
        Stmt *funcBody = func->getBody();

        std::unique_ptr<CFG> sourceCFG = CFG::buildCFG(
            func, funcBody, astContext, clang::CFG::BuildOptions());

        std::string fileName = "sourceCFG_" + std::to_string(numFunctions);

        const clang::CFG *current_cfg = sourceCFG.get();

        if (nullptr != current_cfg)
        {

            StmtPrinterHelper H(
                sourceCFG.get(), astContext->getLangOpts(), astContext);

            GraphHelper = &H;

            WriteGraph(current_cfg, fileName);
        }

        GraphHelper = nullptr;

        return true;
    }
};

class ExampleASTConsumer : public ASTConsumer
{
  private:
    ExampleVisitor *visitor; // doesn't have to be private

  public:
    // override the constructor in order to pass CI
    explicit ExampleASTConsumer(CompilerInstance *CI)
        : visitor(new ExampleVisitor(CI)) // initialize the visitor
    {
    }

    // override this to call our ExampleVisitor on the entire source file
    virtual void HandleTranslationUnit(ASTContext &Context)
    {
        /* we can use ASTContext to get the TranslationUnitDecl, which is
             a single Decl that collectively represents the entire source file
           */
        visitor->TraverseDecl(Context.getTranslationUnitDecl());
    }
};

class ExampleFrontendAction : public ASTFrontendAction
{
  public:
    typedef std::unique_ptr<ASTConsumer> ASTConsumer_unique_ptr_t;

  public:
    virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                           StringRef file)
    {
        return ASTConsumer_unique_ptr_t(
            new ExampleASTConsumer(&CI)); // pass CI pointer to ASTConsumer
    }
};

static cl::OptionCategory MyToolCategory("My tool options");

// CommonOptionsParser declares HelpMessage with a description of the common
// command-line options related to the compilation database and input files.
// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...\n");

int main(int argc, const char **argv)
{
    // parse the command-line args passed to your code
    CommonOptionsParser op(argc, argv, MyToolCategory);
    // create a new Clang Tool instance (a LibTooling environment)
    ClangTool Tool(op.getCompilations(), op.getSourcePathList());

    // run the Clang Tool, creating a new FrontendAction (explained below)
    int result =
        Tool.run(newFrontendActionFactory<ExampleFrontendAction>().get());

    errs() << "\nFound " << numFunctions << " functions.\n\n";

    return result;
}
