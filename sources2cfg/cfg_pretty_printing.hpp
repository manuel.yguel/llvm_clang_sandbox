#include "clang/AST/ASTContext.h"
#include "clang/AST/Attr.h"
#include "clang/AST/Decl.h"
#include "clang/AST/DeclBase.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/DeclGroup.h"
#include "clang/AST/Expr.h"
#include "clang/AST/ExprCXX.h"
#include "clang/AST/OperationKinds.h"
#include "clang/AST/PrettyPrinter.h"
#include "clang/AST/Stmt.h"
#include "clang/AST/StmtCXX.h"
#include "clang/AST/StmtObjC.h"
#include "clang/AST/StmtVisitor.h"
#include "clang/AST/Type.h"
#include "clang/Analysis/CFG.h"
#include "clang/Analysis/ConstructionContext.h"
#include "clang/Analysis/Support/BumpVector.h"
#include "clang/Basic/Builtins.h"
#include "clang/Basic/ExceptionSpecificationType.h"
#include "clang/Basic/LLVM.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/SourceLocation.h"
#include "clang/Basic/Specifiers.h"
#include "llvm/ADT/APInt.h"
#include "llvm/ADT/APSInt.h"
#include "llvm/ADT/ArrayRef.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/Optional.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/Allocator.h"
#include "llvm/Support/Casting.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/DOTGraphTraits.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/Format.h"
#include "llvm/Support/GraphWriter.h"
#include "llvm/Support/SaveAndRestore.h"
#include "llvm/Support/raw_ostream.h"
#include <cassert>
#include <memory>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <iostream>

namespace clang
{

class StmtPrinterHelper : public PrinterHelper
{
    using StmtMapTy =
        llvm::DenseMap<const Stmt *, std::pair<unsigned, unsigned>>;
    using DeclMapTy =
        llvm::DenseMap<const Decl *, std::pair<unsigned, unsigned>>;

    StmtMapTy StmtMap;
    DeclMapTy DeclMap;
    signed currentBlock = 0;
    unsigned currStmt = 0;
    const LangOptions &LangOpts;
    ASTContext *m_astContext;

  public:
    StmtPrinterHelper(const CFG *cfg,
                      const LangOptions &LO,
                      ASTContext *astContext);

    ~StmtPrinterHelper() override = default;

    inline const LangOptions &getLangOpts() const
    {
        return LangOpts;
    }

    inline void setBlockID(signed i)
    {
        currentBlock = i;
    }

    inline void setStmtID(unsigned i)
    {
        currStmt = i;
    }

    inline const ASTContext *astContext() const
    {
        return m_astContext;
    }

    bool handledStmt(Stmt *S, raw_ostream &OS) override;

    bool handleDecl(const Decl *D, raw_ostream &OS);
};

void print_block(raw_ostream &OS,
                 const CFG *cfg,
                 const CFGBlock &B,
                 StmtPrinterHelper &Helper,
                 bool print_edges,
                 bool ShowColors);

extern StmtPrinterHelper *GraphHelper;

} //< namespace clang

namespace llvm
{

template <>
struct DOTGraphTraits<const clang::CFG *> : public DefaultDOTGraphTraits
{

    using GraphType = const clang::CFG *;
    using DOTTraits = DOTGraphTraits<GraphType>;
    using GTraits = GraphTraits<GraphType>;
    using NodeRef = typename GTraits::NodeRef;
    using node_iterator = typename GTraits::nodes_iterator;
    using child_iterator = typename GTraits::ChildIteratorType;

    DOTGraphTraits(bool isSimple = false)
        : DefaultDOTGraphTraits(isSimple)
    {
    }

    static std::string getNodeLabel(const clang::CFGBlock *Node,
                                    const clang::CFG *Graph)
    {
        using namespace clang;
        std::string OutSStr;
        llvm::raw_string_ostream Out(OutSStr);
        if (nullptr != Graph && nullptr != Node)
        {
            print_block(Out, Graph, *Node, *GraphHelper, false, true);
            std::string &OutStr = Out.str();

            if (OutStr[0] == '\n')
                OutStr.erase(OutStr.begin());

            // Process string output to make it nicer...
            for (unsigned i = 0; i != OutStr.length(); ++i)
                if (OutStr[i] == '\n')
                { // Left justify
                    OutStr[i] = '\\';
                    OutStr.insert(OutStr.begin() + i + 1, 'l');
                }
            return OutStr;
        }
        return Out.str();
    }

    enum BranchType
    {
        SwitchBT = 0,
        BinaryBT = 1,
        OtherBT = 2
    };

    static BranchType isBranchOrCaseStmt(const clang::CFGBlock *Node,
                                         const clang::Stmt *cond)
    {
        using namespace clang;
        /** First strategy: try to classify statements to
        know if there are "if type" of "switch type".
        WARNING: does not work with blocks created due to logical conditions
        like: a && b where b is not executed if a is false
        By the way: what kind of statement is that ?

        Stmt *label = const_cast<Stmt *>(cond);
        if (SwitchStmt *S = dyn_cast<SwitchStmt>(label))
        {
            return SwitchBT;
        }
        else if (IfStmt *I = dyn_cast<IfStmt>(label))
        {
            return BinaryBT;
        }
        else if (ForStmt *F = dyn_cast<ForStmt>(label))
        {
            return BinaryBT;
        }
        else if (WhileStmt *W = dyn_cast<WhileStmt>(label))
        {
            return BinaryBT;
        }
        else if (DoStmt *D = dyn_cast<DoStmt>(label))
        {
            return BinaryBT;
        }
        else if( )
        else
        {
            return OtherBT;
        }

        */

        /** Second strategy : count the number of out-edges
          1) 2  : BinaryBT
          2) >2 : SwitchBT
          3) <2 : OtherBT
        */
        size_t succ_size = Node->succ_size();
        if (2 == succ_size)
        { // Handle the case of switch statement with 2 cases (ARGGG)
            Stmt *label = const_cast<Stmt *>(cond);
            if (SwitchStmt *S = dyn_cast<SwitchStmt>(label))
            {
                return SwitchBT;
            }

            return BinaryBT;
        }
        else if (2 < succ_size)
        {
            return SwitchBT;
        }
        else
        {
            return OtherBT;
        }
    }

    static std::string getEdgeSourceLabel(const clang::CFGBlock *Node,
                                          child_iterator I)
    {
        using namespace clang;

        const CFGTerminator &Term = Node->getTerminator();
        if (Term)
        {
            const Stmt *cond = Term.getStmt();
            switch (isBranchOrCaseStmt(Node, cond))
            {
                case BinaryBT:
                { // Label source of conditional branches with "T" or "F"
                    return (I == Node->succ_begin()) ? "T" : "F";
                }
                case SwitchBT:
                { // Label source of switch edges with the associated case
                    // value.
                    std::string Str;
                    raw_string_ostream OS(Str);

                    // evaluate the child: could be a case or default statement
                    Stmt *child = const_cast<Stmt *>((*I)->getLabel());
                    if (isa<DefaultStmt>(child))
                    {
                        return "def";
                    }
                    else if (CaseStmt *C = dyn_cast<CaseStmt>(child))
                    {
                        if (C->getLHS())
                        {
                            C->getLHS()->printPretty(
                                OS,
                                GraphHelper,
                                PrintingPolicy((*GraphHelper).getLangOpts()));
                        }
                    }
                    else
                    {
                        return "";
                    }
                    return OS.str();
                }
                default:
                {
                    return "";
                }
            }
        }

        return "";
    }
};

} // namespace llvm
