
set( MY_LLVM_COMPILED_PATH "/home/manu/buildTree/debug/llvm" CACHE PATH "Path of the clang++ binary to be used." )

set( MY_LLVM_USED_SOURCES "/home/manu/manuLinux/system/llvm_clang/llvm" CACHE PATH "Path of the clang/llvm sources of the version of the clang compiler used." )

# W H I C H   C   C O M P I L E R
#=================================
if( NOT MY_CC_COMPILER_IS_SET )
	set( MY_CC_COMPILER_IS_SET TRUE )
	set( CC_COMPILER ${MY_LLVM_COMPILED_PATH}/bin/clang++ )
endif( NOT MY_CC_COMPILER_IS_SET )

# C   F L A G S
set( CC_DEBUG_FLAGS " -g -Wall -Wstrict-prototypes " )
set( CC_OPTIMIZATION_FLAGS " -Wall -W -O2 -DNDEBUG -msse2 -funroll-loops -Wstrict-prototypes " )
# profiling
#set( CC_DEBUG_FLAGS " -g -pg -Wall -W -Wstrict-prototypes " )
#set( CC_OPTIMIZATION_FLAGS " -Wall -W -O2 -DNDEBUG -msse2  -pg -funroll-loops -Wstrict-prototypes " )

# W H I C H   C++   C O M P I L E R
#==================================
if( NOT MY_CXX_COMPILER_IS_SET )
	set( MY_CXX_COMPILER_IS_SET TRUE )
	set( CXX_COMPILER ${MY_LLVM_COMPILED_PATH}/bin/clang++ )
endif( NOT MY_CXX_COMPILER_IS_SET )

set( CXX_DEBUG_FLAGS " -g -Wall -W -fno-rtti " )
set( CXX_OPTIMIZATION_FLAGS " -Wall -W -O2 -DNDEBUG -msse2 -funroll-loops -fno-rtti " )
#profiling
#set( CXX_DEBUG_FLAGS " -g -pg -Wall -W -fno-rtti " )
#set( CXX_OPTIMIZATION_FLAGS " -Wall -W -O2 -DNDEBUG -msse2  -pg -funroll-loops -fno-rtti " )





#The macros
macro(qt4_find_headers matching_FILES )
	foreach (_current_FILE ${ARGN} )
		get_filename_component(_abs_FILE ${_current_FILE} ABSOLUTE)
		get_source_file_property(_skip ${_abs_FILE} SKIP_AUTOMOC)
		if ( NOT _skip AND EXISTS ${_abs_FILE} )
			file(READ ${_abs_FILE} _contents)
			string(REGEX MATCHALL "Q_OBJECT" _match "${_contents}")
			if(_match)
				set( ${matching_FILES} ${${matching_FILES}} ${_current_FILE} )
			endif(_match)
		endif ( NOT _skip AND EXISTS ${_abs_FILE} )
	endforeach (_current_FILE)
endmacro(qt4_find_headers)

#Init testing
#macro( init_testing )
#  define_property(GLOBAL PROPERTY FAILTEST_FAILURE_COUNT BRIEF_DOCS " " FULL_DOCS " ")
#  define_property(GLOBAL PROPERTY FAILTEST_COUNT BRIEF_DOCS " " FULL_DOCS " ")

#  set_property(GLOBAL PROPERTY FAILTEST_FAILURE_COUNT "0")
#  set_property(GLOBAL PROPERTY FAILTEST_COUNT "0")

#	define_property(GLOBAL PROPERTY COMPILE_TEST_FAILURE_COUNT BRIEF_DOCS " " FULL_DOCS " ")
#  define_property(GLOBAL PROPERTY COMPILE_TEST_COUNT BRIEF_DOCS " " FULL_DOCS " ")

#  set_property(GLOBAL PROPERTY COMPILE_TEST_FAILURE_COUNT "0")
#  set_property(GLOBAL PROPERTY COMPILE_TEST_COUNT "0")
#endmacro( init_testing )


if( NOT COMPILE_TESTS )
  set( COMPILE_TESTS TRUE CACHE BOOL "Enable tests that should simply compile" )
endif( NOT COMPILE_TESTS )

# adds a compiletest, i.e. a test that succeed if the program compiles
# note that the test runner for these is CMake itself
# so here we're just running CMake commands immediately, we're not adding any targets.
# Note: this is a slightly modified version of the Eigen macro.
macro(add_compileTest sources libs bin_dir)
	if(COMMAND cmake_policy)
		if( POLICY CMP0016 )
			cmake_policy(SET CMP0016 OLD)
		endif( POLICY CMP0016 )
	endif(COMMAND cmake_policy)
  if( COMPILE_TESTS )	
		target_link_libraries( ${sources} ${libs} )
		set( EXECUTABLE_OUTPUT_PATH ${bin_dir} )

		#math(EXPR COMPILE_TEST_COUNT ${FAILTEST_COUNT}+1)

		#    set_property(GLOBAL PROPERTY COMPILE_TEST_FAILURE_COUNT ${COMPILE_TEST_FAILURE_COUNT})
		#    set_property(GLOBAL PROPERTY COMPILE_TEST_COUNT ${COMPILE_TEST_COUNT})
  endif( COMPILE_TESTS )
endmacro(add_compileTest)






if( NOT FAIL_COMPILE_TESTS )
  set( FAIL_COMPILE_TESTS TRUE CACHE BOOL "Enable tests that should fail to compile" )
endif( NOT FAIL_COMPILE_TESTS )

# adds a failtest, i.e. a test that succeed if the program fails to compile
# note that the test runner for these is CMake itself
# so here we're just running CMake commands immediately, we're not adding any targets.
# Note: this is a slightly modified version of the Eigen macro.
macro(add_failTest testname)
  if( FAIL_COMPILE_TESTS )
		#    get_property( FAILTEST_FAILURE_COUNT GLOBAL PROPERTY FAILTEST_FAILURE_COUNT)
		#    get_property( FAILTEST_COUNT GLOBAL PROPERTY FAILTEST_COUNT)

    message(STATUS "" )
    message(STATUS "Checking failtest: ${testname}")
    set(filename "${testname}")
    file(READ "${filename}" test_source)

    try_compile(succeeds_when_it_should_fail
        "${CMAKE_CURRENT_BINARY_DIR}"
        "${filename}"
        COMPILE_DEFINITIONS )
    if (succeeds_when_it_should_fail)
      message(STATUS "FAILED: ${testname} build succeeded when it should have failed")
			#math(EXPR FAILTEST_FAILURE_COUNT ${FAILTEST_FAILURE_COUNT}+1)
    endif()

		#math(EXPR FAILTEST_COUNT ${FAILTEST_COUNT}+1)

		#    set_property(GLOBAL PROPERTY FAILTEST_FAILURE_COUNT ${FAILTEST_FAILURE_COUNT})
		#    set_property(GLOBAL PROPERTY FAILTEST_COUNT ${FAILTEST_COUNT})
  endif( FAIL_COMPILE_TESTS )
endmacro(add_failTest)



if( NOT BUILD_UNIT_TESTS )
	set( BUILD_UNIT_TESTS TRUE CACHE BOOL "Enable the compilation of the unit tests." )
endif( NOT BUILD_UNIT_TESTS )

if( NOT BUILD_LIBRARY_TESTS )
  set( BUILD_LIBRARY_TESTS TRUE CACHE BOOL "Enable the compilation of the programs known as library tests. They differ from the unit tests since they test complex behaviours of a library component involving several functionalities." )
endif( NOT BUILD_LIBRARY_TESTS )

if( NOT BUILD_LIBRARY_TOOLS )
  set( BUILD_LIBRARY_TOOLS TRUE CACHE BOOL "Enable the compilation of the programs known as tools usually defined in libraries." )
endif( NOT BUILD_LIBRARY_TOOLS )

