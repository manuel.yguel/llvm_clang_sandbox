//===- CFG.cpp - Classes for representing and building CFGs ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file defines the CFG and CFGBuilder classes for representing and
//  building Control-Flow Graphs (CFGs) from ASTs.
//
//===----------------------------------------------------------------------===//

#include "cfg_pretty_printing.hpp"

using namespace clang;

//===----------------------------------------------------------------------===//
// CFG pretty printing
//===----------------------------------------------------------------------===//

StmtPrinterHelper::StmtPrinterHelper(const CFG *cfg,
                                     const LangOptions &LO,
                                     ASTContext *astContext)
    : LangOpts(LO)
    , m_astContext(astContext)
{
    if (nullptr != cfg)
    {
        for (CFG::const_iterator I = cfg->begin(), E = cfg->end(); I != E; ++I)
        {
            unsigned j = 1;
            for (CFGBlock::const_iterator BI = (*I)->begin(),
                                          BEnd = (*I)->end();
                 BI != BEnd;
                 ++BI, ++j)
            {
                if (Optional<CFGStmt> SE = BI->getAs<CFGStmt>())
                {
                    const Stmt *stmt = SE->getStmt();
                    std::pair<unsigned, unsigned> P((*I)->getBlockID(), j);
                    StmtMap[stmt] = P;

                    switch (stmt->getStmtClass())
                    {
                        case Stmt::DeclStmtClass:
                            DeclMap[cast<DeclStmt>(stmt)->getSingleDecl()] = P;
                            break;
                        case Stmt::IfStmtClass:
                        {
                            const VarDecl *var =
                                cast<IfStmt>(stmt)->getConditionVariable();
                            if (var)
                                DeclMap[var] = P;
                            break;
                        }
                        case Stmt::ForStmtClass:
                        {
                            const VarDecl *var =
                                cast<ForStmt>(stmt)->getConditionVariable();
                            if (var)
                                DeclMap[var] = P;
                            break;
                        }
                        case Stmt::WhileStmtClass:
                        {
                            const VarDecl *var =
                                cast<WhileStmt>(stmt)->getConditionVariable();
                            if (var)
                                DeclMap[var] = P;
                            break;
                        }
                        case Stmt::SwitchStmtClass:
                        {
                            const VarDecl *var =
                                cast<SwitchStmt>(stmt)->getConditionVariable();
                            if (var)
                                DeclMap[var] = P;
                            break;
                        }
                        case Stmt::CXXCatchStmtClass:
                        {
                            const VarDecl *var =
                                cast<CXXCatchStmt>(stmt)->getExceptionDecl();
                            if (var)
                                DeclMap[var] = P;
                            break;
                        }
                        default:
                            break;
                    }
                }
            }
        }
    }
}

bool StmtPrinterHelper::handledStmt(Stmt *S, raw_ostream &OS)
{
    StmtMapTy::iterator I = StmtMap.find(S);

    if (I == StmtMap.end())
        return false;

    if (currentBlock >= 0 && I->second.first == (unsigned)currentBlock &&
        I->second.second == currStmt)
    {
        return false;
    }

    OS << "[B" << I->second.first << "." << I->second.second << "]";
    return true;
}

bool StmtPrinterHelper::handleDecl(const Decl *D, raw_ostream &OS)
{
    DeclMapTy::iterator I = DeclMap.find(D);

    if (I == DeclMap.end())
        return false;

    if (currentBlock >= 0 && I->second.first == (unsigned)currentBlock &&
        I->second.second == currStmt)
    {
        return false;
    }

    OS << "[B" << I->second.first << "." << I->second.second << "]";
    return true;
}

class CFGBlockTerminatorPrint
    : public StmtVisitor<CFGBlockTerminatorPrint, void>
{
    raw_ostream &OS;
    StmtPrinterHelper *Helper;
    PrintingPolicy Policy;

  public:
    CFGBlockTerminatorPrint(raw_ostream &os,
                            StmtPrinterHelper *helper,
                            const PrintingPolicy &Policy)
        : OS(os)
        , Helper(helper)
        , Policy(Policy)
    {
        this->Policy.IncludeNewlines = false;
    }

    void VisitIfStmt(IfStmt *I)
    {
        OS << "if ";
        if (Stmt *C = I->getCond())
            C->printPretty(OS, Helper, Policy);
    }

    // Default case.
    void VisitStmt(Stmt *Terminator)
    {
        Terminator->printPretty(OS, Helper, Policy);
    }

    void VisitDeclStmt(DeclStmt *DS)
    {
        VarDecl *VD = cast<VarDecl>(DS->getSingleDecl());
        OS << "static init " << VD->getName();
    }

    void VisitForStmt(ForStmt *F)
    {
        OS << "for (";
        if (F->getInit())
            OS << "...";
        OS << "; ";
        if (Stmt *C = F->getCond())
            C->printPretty(OS, Helper, Policy);
        OS << "; ";
        if (F->getInc())
            OS << "...";
        OS << ")";
    }

    void VisitWhileStmt(WhileStmt *W)
    {
        OS << "while ";
        if (Stmt *C = W->getCond())
            C->printPretty(OS, Helper, Policy);
    }

    void VisitDoStmt(DoStmt *D)
    {
        OS << "do ... while ";
        if (Stmt *C = D->getCond())
            C->printPretty(OS, Helper, Policy);
    }

    void VisitSwitchStmt(SwitchStmt *Terminator)
    {
        OS << "switch ";
        Terminator->getCond()->printPretty(OS, Helper, Policy);
    }

    void VisitCXXTryStmt(CXXTryStmt *CS)
    {
        OS << "try ...";
    }

    void VisitSEHTryStmt(SEHTryStmt *CS)
    {
        OS << "__try ...";
    }

    void VisitAbstractConditionalOperator(AbstractConditionalOperator *C)
    {
        if (Stmt *Cond = C->getCond())
            Cond->printPretty(OS, Helper, Policy);
        OS << " ? ... : ...";
    }

    void VisitChooseExpr(ChooseExpr *C)
    {
        OS << "__builtin_choose_expr( ";
        if (Stmt *Cond = C->getCond())
            Cond->printPretty(OS, Helper, Policy);
        OS << " )";
    }

    void VisitIndirectGotoStmt(IndirectGotoStmt *I)
    {
        OS << "goto *";
        if (Stmt *T = I->getTarget())
            T->printPretty(OS, Helper, Policy);
    }

    void VisitBinaryOperator(BinaryOperator *B)
    {
        if (!B->isLogicalOp())
        {
            VisitExpr(B);
            return;
        }

        if (B->getLHS())
            B->getLHS()->printPretty(OS, Helper, Policy);

        switch (B->getOpcode())
        {
            case BO_LOr:
                OS << " || ...";
                return;
            case BO_LAnd:
                OS << " && ...";
                return;
            default:
                llvm_unreachable("Invalid logical operator.");
        }
    }

    void VisitExpr(Expr *E)
    {
        E->printPretty(OS, Helper, Policy);
    }

  public:
    void print(CFGTerminator T)
    {
        if (T.isTemporaryDtorsBranch())
            OS << "(Temp Dtor) ";
        Visit(T.getStmt());
    }
};

void print_initializer(raw_ostream &OS,
                       StmtPrinterHelper &Helper,
                       const CXXCtorInitializer *I)
{
    if (I->isBaseInitializer())
        OS << I->getBaseClass()->getAsCXXRecordDecl()->getName();
    else if (I->isDelegatingInitializer())
        OS << I->getTypeSourceInfo()
                  ->getType()
                  ->getAsCXXRecordDecl()
                  ->getName();
    else
        OS << I->getAnyMember()->getName();
    OS << "(";
    if (Expr *IE = I->getInit())
        IE->printPretty(OS, &Helper, PrintingPolicy(Helper.getLangOpts()));
    OS << ")";

    if (I->isBaseInitializer())
        OS << " (Base initializer)";
    else if (I->isDelegatingInitializer())
        OS << " (Delegating initializer)";
    else
        OS << " (Member initializer)";
}

void print_construction_context(raw_ostream &OS,
                                StmtPrinterHelper &Helper,
                                const ConstructionContext *CC)
{
    const Stmt *S1 = nullptr, *S2 = nullptr;
    switch (CC->getKind())
    {
        case ConstructionContext::SimpleConstructorInitializerKind:
        {
            OS << ", ";
            const auto *SICC =
                cast<SimpleConstructorInitializerConstructionContext>(CC);
            print_initializer(OS, Helper, SICC->getCXXCtorInitializer());
            break;
        }
        case ConstructionContext::CXX17ElidedCopyConstructorInitializerKind:
        {
            OS << ", ";
            const auto *CICC =
                cast<CXX17ElidedCopyConstructorInitializerConstructionContext>(
                    CC);
            print_initializer(OS, Helper, CICC->getCXXCtorInitializer());
            S2 = CICC->getCXXBindTemporaryExpr();
            break;
        }
        case ConstructionContext::SimpleVariableKind:
        {
            const auto *SDSCC = cast<SimpleVariableConstructionContext>(CC);
            S1 = SDSCC->getDeclStmt();
            break;
        }
        case ConstructionContext::CXX17ElidedCopyVariableKind:
        {
            const auto *CDSCC =
                cast<CXX17ElidedCopyVariableConstructionContext>(CC);
            S1 = CDSCC->getDeclStmt();
            S2 = CDSCC->getCXXBindTemporaryExpr();
            break;
        }
        case ConstructionContext::NewAllocatedObjectKind:
        {
            const auto *NECC = cast<NewAllocatedObjectConstructionContext>(CC);
            S1 = NECC->getCXXNewExpr();
            break;
        }
        case ConstructionContext::SimpleReturnedValueKind:
        {
            const auto *RSCC = cast<SimpleReturnedValueConstructionContext>(CC);
            S1 = RSCC->getReturnStmt();
            break;
        }
        case ConstructionContext::CXX17ElidedCopyReturnedValueKind:
        {
            const auto *RSCC =
                cast<CXX17ElidedCopyReturnedValueConstructionContext>(CC);
            S1 = RSCC->getReturnStmt();
            S2 = RSCC->getCXXBindTemporaryExpr();
            break;
        }
        case ConstructionContext::TemporaryObjectKind:
        {
            const auto *TOCC = cast<TemporaryObjectConstructionContext>(CC);
            S1 = TOCC->getCXXBindTemporaryExpr();
            S2 = TOCC->getMaterializedTemporaryExpr();
            break;
        }
    }
    if (S1)
    {
        OS << ", ";
        Helper.handledStmt(const_cast<Stmt *>(S1), OS);
    }
    if (S2)
    {
        OS << ", ";
        Helper.handledStmt(const_cast<Stmt *>(S2), OS);
    }
}

void print_elem(raw_ostream &OS, StmtPrinterHelper &Helper, const CFGElement &E)
{
    if (Optional<CFGStmt> CS = E.getAs<CFGStmt>())
    {
        const Stmt *S = CS->getStmt();
        assert(S != nullptr && "Expecting non-null Stmt");

        // Print File name, line number, column number
        /*
        SourceLocation src_loc_start = S->getLocStart();
        OS << src_loc_start.printToString(
                  Helper.astContext()->getSourceManager())
           << "   ";
        */

        // special printing for statement-expressions.
        if (const StmtExpr *SE = dyn_cast<StmtExpr>(S))
        {
            const CompoundStmt *Sub = SE->getSubStmt();

            auto Children = Sub->children();
            if (Children.begin() != Children.end())
            {
                OS << "({ ... ; ";
                Helper.handledStmt(*SE->getSubStmt()->body_rbegin(), OS);
                OS << " })\n";
                return;
            }
        }
        // special printing for comma expressions.
        if (const BinaryOperator *B = dyn_cast<BinaryOperator>(S))
        {
            if (B->getOpcode() == BO_Comma)
            {
                OS << "... , ";
                Helper.handledStmt(B->getRHS(), OS);
                OS << '\n';
                return;
            }
        }
        S->printPretty(OS, &Helper, PrintingPolicy(Helper.getLangOpts()));

        if (auto VTC = E.getAs<CFGCXXRecordTypedCall>())
        {
            if (isa<CXXOperatorCallExpr>(S))
                OS << " (OperatorCall)";
            OS << " (CXXRecordTypedCall";
            print_construction_context(
                OS, Helper, VTC->getConstructionContext());
            OS << ")";
        }
        else if (isa<CXXOperatorCallExpr>(S))
        {
            OS << " (OperatorCall)";
        }
        else if (isa<CXXBindTemporaryExpr>(S))
        {
            OS << " (BindTemporary)";
        }
        else if (const CXXConstructExpr *CCE = dyn_cast<CXXConstructExpr>(S))
        {
            OS << " (CXXConstructExpr";
            if (Optional<CFGConstructor> CE = E.getAs<CFGConstructor>())
            {
                print_construction_context(
                    OS, Helper, CE->getConstructionContext());
            }
            OS << ", " << CCE->getType().getAsString() << ")";
        }
        else if (const CastExpr *CE = dyn_cast<CastExpr>(S))
        {
            OS << " (" << CE->getStmtClassName() << ", "
               << CE->getCastKindName() << ", " << CE->getType().getAsString()
               << ")";
        }

        // Expressions need a newline.
        if (isa<Expr>(S))
            OS << '\n';
    }
    else if (Optional<CFGInitializer> IE = E.getAs<CFGInitializer>())
    {
        print_initializer(OS, Helper, IE->getInitializer());
        OS << '\n';
    }
    else if (Optional<CFGAutomaticObjDtor> DE = E.getAs<CFGAutomaticObjDtor>())
    {
        const VarDecl *VD = DE->getVarDecl();
        Helper.handleDecl(VD, OS);

        const Type *T = VD->getType().getTypePtr();
        if (const ReferenceType *RT = T->getAs<ReferenceType>())
            T = RT->getPointeeType().getTypePtr();
        T = T->getBaseElementTypeUnsafe();

        OS << ".~" << T->getAsCXXRecordDecl()->getName().str() << "()";
        OS << " (Implicit destructor)\n";
    }
    else if (Optional<CFGLifetimeEnds> DE = E.getAs<CFGLifetimeEnds>())
    {
        const VarDecl *VD = DE->getVarDecl();
        Helper.handleDecl(VD, OS);

        OS << " (Lifetime ends)\n";
    }
    else if (Optional<CFGLoopExit> LE = E.getAs<CFGLoopExit>())
    {
        const Stmt *LoopStmt = LE->getLoopStmt();
        OS << LoopStmt->getStmtClassName() << " (LoopExit)\n";
    }
    else if (Optional<CFGScopeBegin> SB = E.getAs<CFGScopeBegin>())
    {
        OS << "CFGScopeBegin(";
        if (const VarDecl *VD = SB->getVarDecl())
            OS << VD->getQualifiedNameAsString();
        OS << ")\n";
    }
    else if (Optional<CFGScopeEnd> SE = E.getAs<CFGScopeEnd>())
    {
        OS << "CFGScopeEnd(";
        if (const VarDecl *VD = SE->getVarDecl())
            OS << VD->getQualifiedNameAsString();
        OS << ")\n";
    }
    else if (Optional<CFGNewAllocator> NE = E.getAs<CFGNewAllocator>())
    {
        OS << "CFGNewAllocator(";
        if (const CXXNewExpr *AllocExpr = NE->getAllocatorExpr())
            AllocExpr->getType().print(OS,
                                       PrintingPolicy(Helper.getLangOpts()));
        OS << ")\n";
    }
    else if (Optional<CFGDeleteDtor> DE = E.getAs<CFGDeleteDtor>())
    {
        const CXXRecordDecl *RD = DE->getCXXRecordDecl();
        if (!RD)
            return;
        CXXDeleteExpr *DelExpr =
            const_cast<CXXDeleteExpr *>(DE->getDeleteExpr());
        Helper.handledStmt(cast<Stmt>(DelExpr->getArgument()), OS);
        OS << "->~" << RD->getName().str() << "()";
        OS << " (Implicit destructor)\n";
    }
    else if (Optional<CFGBaseDtor> BE = E.getAs<CFGBaseDtor>())
    {
        const CXXBaseSpecifier *BS = BE->getBaseSpecifier();
        OS << "~" << BS->getType()->getAsCXXRecordDecl()->getName() << "()";
        OS << " (Base object destructor)\n";
    }
    else if (Optional<CFGMemberDtor> ME = E.getAs<CFGMemberDtor>())
    {
        const FieldDecl *FD = ME->getFieldDecl();
        const Type *T = FD->getType()->getBaseElementTypeUnsafe();
        OS << "this->" << FD->getName();
        OS << ".~" << T->getAsCXXRecordDecl()->getName() << "()";
        OS << " (Member object destructor)\n";
    }
    else if (Optional<CFGTemporaryDtor> TE = E.getAs<CFGTemporaryDtor>())
    {
        const CXXBindTemporaryExpr *BT = TE->getBindTemporaryExpr();
        OS << "~";
        BT->getType().print(OS, PrintingPolicy(Helper.getLangOpts()));
        OS << "() (Temporary object destructor)\n";
    }
}

void clang::print_block(raw_ostream &OS,
                        const CFG *cfg,
                        const CFGBlock &B,
                        StmtPrinterHelper &Helper,
                        bool print_edges,
                        bool ShowColors)
{
    Helper.setBlockID(B.getBlockID());

    // Print the header.
    if (ShowColors)
    {
        OS.changeColor(raw_ostream::YELLOW, true);
    }

    OS << "\n [B" << B.getBlockID();

    if (&B == &cfg->getEntry())
        OS << " (ENTRY)]\n";
    else if (&B == &cfg->getExit())
        OS << " (EXIT)]\n";
    else if (&B == cfg->getIndirectGotoBlock())
        OS << " (INDIRECT GOTO DISPATCH)]\n";
    else if (B.hasNoReturnElement())
        OS << " (NORETURN)]\n";
    else
        OS << "]\n";

    if (ShowColors)
        OS.resetColor();

    // Print the label of this block.
    if (Stmt *Label = const_cast<Stmt *>(B.getLabel()))
    {
        if (print_edges)
            OS << "  ";

        if (LabelStmt *L = dyn_cast<LabelStmt>(Label))
            OS << L->getName();
        else if (CaseStmt *C = dyn_cast<CaseStmt>(Label))
        {
            OS << "case ";
            if (C->getLHS())
                C->getLHS()->printPretty(
                    OS, &Helper, PrintingPolicy(Helper.getLangOpts()));
            if (C->getRHS())
            {
                OS << " ... ";
                C->getRHS()->printPretty(
                    OS, &Helper, PrintingPolicy(Helper.getLangOpts()));
            }
        }
        else if (isa<DefaultStmt>(Label))
            OS << "default";
        else if (CXXCatchStmt *CS = dyn_cast<CXXCatchStmt>(Label))
        {
            OS << "catch (";
            if (CS->getExceptionDecl())
                CS->getExceptionDecl()->print(
                    OS, PrintingPolicy(Helper.getLangOpts()), 0);
            else
                OS << "...";
            OS << ")";
        }
        else if (SEHExceptStmt *ES = dyn_cast<SEHExceptStmt>(Label))
        {
            OS << "__except (";
            ES->getFilterExpr()->printPretty(
                OS, &Helper, PrintingPolicy(Helper.getLangOpts()), 0);
            OS << ")";
        }
        else
            llvm_unreachable("Invalid label statement in CFGBlock.");

        OS << ":\n";
    }

    // Iterate through the statements in the block and print them.
    unsigned j = 1;

    for (CFGBlock::const_iterator I = B.begin(), E = B.end(); I != E; ++I, ++j)
    {
        // Print the statement # in the basic block and the statement itself.
        if (print_edges)
            OS << " ";

        OS << llvm::format("%3d", j) << ": ";

        Helper.setStmtID(j);

        print_elem(OS, Helper, *I);
    }

    // Print the terminator of this block.
    if (B.getTerminator())
    {
        if (ShowColors)
            OS.changeColor(raw_ostream::GREEN);

        OS << "   Test: ";

        Helper.setBlockID(-1);

        PrintingPolicy PP(Helper.getLangOpts());
        CFGBlockTerminatorPrint TPrinter(OS, &Helper, PP);
        TPrinter.print(B.getTerminator());
        OS << '\n';

        if (ShowColors)
            OS.resetColor();
    }

    if (print_edges)
    {
        // Print the predecessors of this block.
        if (!B.pred_empty())
        {
            const raw_ostream::Colors Color = raw_ostream::BLUE;
            if (ShowColors)
                OS.changeColor(Color);
            OS << "   Preds ";
            if (ShowColors)
                OS.resetColor();
            OS << '(' << B.pred_size() << "):";
            unsigned i = 0;

            if (ShowColors)
                OS.changeColor(Color);

            for (CFGBlock::const_pred_iterator I = B.pred_begin(),
                                               E = B.pred_end();
                 I != E;
                 ++I, ++i)
            {
                if (i % 10 == 8)
                    OS << "\n     ";

                CFGBlock *B = *I;
                bool Reachable = true;
                if (!B)
                {
                    Reachable = false;
                    B = I->getPossiblyUnreachableBlock();
                }

                OS << " B" << B->getBlockID();
                if (!Reachable)
                    OS << "(Unreachable)";
            }

            if (ShowColors)
                OS.resetColor();

            OS << '\n';
        }

        // Print the successors of this block.
        if (!B.succ_empty())
        {
            const raw_ostream::Colors Color = raw_ostream::MAGENTA;
            if (ShowColors)
                OS.changeColor(Color);
            OS << "   Succs ";
            if (ShowColors)
                OS.resetColor();
            OS << '(' << B.succ_size() << "):";
            unsigned i = 0;

            if (ShowColors)
                OS.changeColor(Color);

            for (CFGBlock::const_succ_iterator I = B.succ_begin(),
                                               E = B.succ_end();
                 I != E;
                 ++I, ++i)
            {
                if (i % 10 == 8)
                    OS << "\n    ";

                CFGBlock *B = *I;

                bool Reachable = true;
                if (!B)
                {
                    Reachable = false;
                    B = I->getPossiblyUnreachableBlock();
                }

                if (B)
                {
                    OS << " B" << B->getBlockID();
                    if (!Reachable)
                        OS << "(Unreachable)";
                }
                else
                {
                    OS << " NULL";
                }
            }

            if (ShowColors)
                OS.resetColor();
            OS << '\n';
        }
    }
}

clang::StmtPrinterHelper *clang::GraphHelper = nullptr;